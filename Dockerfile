FROM python:3.5

MAINTAINER Spoqa

RUN apt-get update && \
    apt-get install -y postgresql && \
    rm -rf /var/lib/apt/lists/*

COPY pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
